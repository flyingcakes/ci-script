# CI Scripts

This repository houses CI scripts for Flatpak builds. They are intended to be used with KDE repositories.

## Using

To use this in your KDE application, follow these steps

1. Ensure the Flatpak manifest for your application is there on [Flatpak KDE Applications](https://invent.kde.org/packaging/flatpak-kde-applications/) repo. Manifests may be Remote app,  JSON or YAML file.
2. Add the following include in your application repository's `.gitlab-ci.yml`
```yml
include: 'https://gitlab.com/flyingcakes/ci-script/-/raw/main/deps-build-v2.yml'
```
3. Add this block in your application repository's `.gitlab-ci.yml`
```yml
flatpak:
 extends: '.flatpak'
 variables:
 FILE_NAME: org.kde.filename
 MODULE_NAME: module
 APP_ID: org.kde.appid
```

`FILE_NAME` is to be set to the name of file of your application's manifest in [Flatpak KDE Applications](https://invent.kde.org/packaging/flatpak-kde-applications/) repo. Make sure to add the right extension. Eg.
```yml
FILE_NAME: org.kde.kdiff3.json
```

`MODULE_NAME` is the name of your application's module in the manifest. Eg.
```yml
MODULE_NAME: kdiff3
```

`APP_ID` is the name with which your application bundle will be created post successful build. Eg
```yml
APP_ID: org.kde.kdiff3
```

## Deployments

To test this script, I have deployed it on these repositories

1. KDiff3
	- Link : https://gitlab.com/flyingcakes/kdiff
	- Manifest type : JSON
	- Job Run : https://gitlab.com/flyingcakes/kdiff/-/jobs/2299449380
2. Kronometer
	- Link : https://gitlab.com/flyingcakes/kronometer
	- Manifest type : Remote Application
	- Job Run : https://gitlab.com/flyingcakes/kronometer/-/jobs/2299408514

## Working

The latest files are `deps-build-v2.yml` and `deps-build-v2`.

1. The entry point is in `deps-build-v2.yml`. It starts a docker container and fetches the `deps-build` python script.
2. The python script takes over to build the application.
3. First it checks if the manifest is a remote application. In that case, it fetches the git repository and updates `manifestfile` location accordingly. If manifest is a JSON or YAML file, this step is skipped.
4. Next, the manifest is edited to use local directory as the build source. (rather than the git repository on website)
5. Application is now built. After successful build, python script exits.
6. Back in `deps-build-v2.yml`, it prepares artifacts and uploads it. These artifacts can be downloaded by maintainers to test the application.
